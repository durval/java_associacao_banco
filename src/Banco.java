import java.text.SimpleDateFormat;
import java.util.Date;

public class Banco {

	public static void main(String[] args) {
		
		ContaCorrente contaUniversitaria = new ContaCorrente(3000.0f);
		Transacao transacoes = new Transacao();
		
		float saldoAtual;
		
		saldoAtual = contaUniversitaria.getSaldo();
	
		transacoes.realizarTransacao(new Date(), contaUniversitaria, "Conta do Celular", 40.0f, Movimento.SACAR);
		transacoes.realizarTransacao(new Date(), contaUniversitaria, "Recarregar cartão do RU", 20.0f, Movimento.SACAR);
		transacoes.realizarTransacao(new Date(), contaUniversitaria, "Pagar Aluguel", 450.0f, Movimento.SACAR);
		transacoes.realizarTransacao(new Date(), contaUniversitaria, "Receber Bolsa da Monitoria", 450.0f, Movimento.DEPOSITAR);
		transacoes.realizarTransacao(new Date(), contaUniversitaria, "Receber Auxílio", 400.0f, Movimento.DEPOSITAR);
		
		System.out.println("Extrato da Conta");
		System.out.println("---------------------------");
		System.out.println("Saldo Inicial: " + saldoAtual);
		System.out.println("---------------------------");
		System.out.println("Data \t    Valor   \t Tipo de Operação  \t   Saldo Acumulado \t  Histórico" );
		for(Movimento mov: transacoes.getMovimento()){
			System.out.print(new SimpleDateFormat("dd/MM/yyyy").format(mov.getData())); 
			System.out.print(" " + mov.getValor());
			System.out.print(" " + (mov.getOperacao() == Movimento.DEPOSITAR ? "Depósito" : "Saque"));
			if(mov.getOperacao() == Movimento.DEPOSITAR) {
				saldoAtual += mov.getValor();
			}
			else
				saldoAtual -= mov.getValor();
			System.out.print(" " + saldoAtual);
			System.out.println(" " + mov.getHistorico());
		}
		System.out.println("---------------------------");
		System.out.println( "Saldo Final: " + contaUniversitaria.getSaldo());
		
	}

}
